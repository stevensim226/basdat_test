from django.shortcuts import render
from django.http import HttpResponse
from django.db import connection

def session_view(request):
    if request.method == "POST":
        request.session["username"] = "Matsuzaka"

    print(dict(request.session))
    try:
        username = request.session["username"]
    except KeyError:
        username = "Anonymous"

    return render(request, "medikago/index.html", context={
        "username" : username
    })

# Docs cursor ada di https://docs.djangoproject.com/en/3.1/topics/db/sql/
def people_view(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM people")
        row = cursor.fetchall()
        print(row)
        return HttpResponse(f"Yeah, {row}")
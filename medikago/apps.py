from django.apps import AppConfig


class MedikagoConfig(AppConfig):
    name = 'medikago'
